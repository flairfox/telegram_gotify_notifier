package ru.blodge.bserver.telegram_gotify_notifier.telegram

import io.github.oshai.kotlinlogging.KotlinLogging
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service
import org.telegram.telegrambots.bots.TelegramLongPollingBot
import org.telegram.telegrambots.meta.api.methods.send.SendMessage
import org.telegram.telegrambots.meta.api.objects.Update
import ru.blodge.bserver.telegram_gotify_notifier.gotify.GotifyMessage

@Service
class TelegramMessageService(
    @Value("\${telegram.bot-token}") botToken: String,
    @Value("\${telegram.bot-username}") private val botUsername: String,
    @Value("\${telegram.admin-user-ids}") private val adminUserIds: List<Long>,
    @Autowired private val chatIdService: ChatIdService
) : TelegramLongPollingBot(botToken) {

    private val logger = KotlinLogging.logger {}

    override fun getBotUsername(): String = botUsername

    override fun onUpdateReceived(update: Update) {

        if (chatIdService.chatId == null &&
            update.hasMessage() &&
            adminUserIds.contains(update.message.from.id) &&
            update.message.text == "/start") {

            logger.info { "Saving new chatId" }
            chatIdService.saveChatId(update.message.chatId)
        }

    }

    fun sendMessage(gotifyMessage: GotifyMessage) {
        if (chatIdService.chatId != null) {
            val message = SendMessage()
            message.chatId = chatIdService.chatId.toString()
            message.parseMode = "markdown"
            message.text = """
                *${gotifyMessage.title}*
                ${gotifyMessage.message}
            """.trimIndent()

            execute(message)
        }
    }

}