package ru.blodge.bserver.telegram_gotify_notifier.telegram

import io.github.oshai.kotlinlogging.KotlinLogging
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import ru.blodge.bserver.telegram_gotify_notifier.persistence.entities.PersistedParameter
import ru.blodge.bserver.telegram_gotify_notifier.persistence.repositories.PersistedParameterRepository

@Service
class ChatIdService(
    @Autowired private val persistedParameterRepository: PersistedParameterRepository) {

    private val logger = KotlinLogging.logger {}

    private var chatIdCache: Long? = null
    val chatId: Long?
        get() {
            if (chatIdCache == null)
                chatIdCache = loadChatId()

            return chatIdCache
        }

    private fun loadChatId(): Long? {
        return try {
            persistedParameterRepository
                .getReferenceById("chatId")
                .parameterValue
                .toLong()
        } catch (e: Exception) {
            logger.info { "Parameter chatId not found!" }
            null
        }
    }

    fun saveChatId(chatId: Long) {
        persistedParameterRepository.save(PersistedParameter("chatId", chatId.toString()))
    }

}