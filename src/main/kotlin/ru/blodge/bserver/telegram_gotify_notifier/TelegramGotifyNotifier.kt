package ru.blodge.bserver.telegram_gotify_notifier

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class TelegramGotifyNotifier

fun main(args: Array<String>) {
    runApplication<TelegramGotifyNotifier>(*args)
}
