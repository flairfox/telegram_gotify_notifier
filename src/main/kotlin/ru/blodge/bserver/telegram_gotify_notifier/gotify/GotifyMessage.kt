package ru.blodge.bserver.telegram_gotify_notifier.gotify

import java.time.LocalDateTime

data class GotifyMessage(
    val appid: Int,
    val date: LocalDateTime,
    val extras: Any?,
    val id: Int,
    val message: String,
    val priority: Int,
    val title: String
)
