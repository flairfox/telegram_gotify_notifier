package ru.blodge.bserver.telegram_gotify_notifier.gotify

import com.fasterxml.jackson.databind.ObjectMapper
import io.github.oshai.kotlinlogging.KotlinLogging
import jakarta.annotation.PostConstruct
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service
import org.springframework.web.socket.*
import org.springframework.web.socket.client.standard.StandardWebSocketClient
import org.springframework.web.socket.handler.TextWebSocketHandler
import ru.blodge.bserver.telegram_gotify_notifier.telegram.TelegramMessageService
import java.net.URI

@Service
class GotifyClient(
    @Value("\${gotify.url}") private val gotifyUrl: String,
    @Value("\${gotify.token}") private val gotifyClientToken: String,
    @Autowired private val objectMapper: ObjectMapper,
    @Autowired private val messageService: TelegramMessageService
) {

    private val logger = KotlinLogging.logger {}

    @PostConstruct
    fun connect() {
        try {
            val webSocketClient = StandardWebSocketClient()
            val headers = WebSocketHttpHeaders()
            headers.set("X-Gotify-Key", gotifyClientToken)

            webSocketClient.execute(object : TextWebSocketHandler() {

                override fun afterConnectionEstablished(session: WebSocketSession) {
                    logger.info { "Connection with Gotify server $gotifyUrl is established" }
                }

                override fun handleTextMessage(session: WebSocketSession, message: TextMessage) {
                    val gotifyMessage = objectMapper.readValue(message.payload, GotifyMessage::class.java)
                    logger.info { "Received Gotify message $gotifyMessage" }

                    messageService.sendMessage(gotifyMessage)
                }

                override fun handleTransportError(session: WebSocketSession, exception: Throwable) {
                    logger.error(exception) { "Gotify server connection error" }
                }

                override fun afterConnectionClosed(session: WebSocketSession, closeStatus: CloseStatus) {
                    logger.info { "Connection with Gotify server closed with status ${closeStatus.reason}" }
                }

                override fun supportsPartialMessages(): Boolean = false

            }, headers, URI.create("ws://$gotifyUrl/stream")).get()
        } catch (e: Exception) {
            logger.error(e) { "Connection error" }
        }
    }

}