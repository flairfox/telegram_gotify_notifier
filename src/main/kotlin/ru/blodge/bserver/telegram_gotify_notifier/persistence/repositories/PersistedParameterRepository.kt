package ru.blodge.bserver.telegram_gotify_notifier.persistence.repositories

import org.springframework.data.jpa.repository.JpaRepository
import ru.blodge.bserver.telegram_gotify_notifier.persistence.entities.PersistedParameter

interface PersistedParameterRepository : JpaRepository<PersistedParameter, String>