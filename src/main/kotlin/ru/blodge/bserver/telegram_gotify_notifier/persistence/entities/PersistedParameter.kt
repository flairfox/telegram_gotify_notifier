package ru.blodge.bserver.telegram_gotify_notifier.persistence.entities

import jakarta.persistence.Column
import jakarta.persistence.Entity
import jakarta.persistence.Id
import jakarta.persistence.Table

@Entity
@Table(name = "persisted_parameters")
data class PersistedParameter(
    @Id
    @Column(name = "parameter_name")
    val parameterName: String,

    @Column(name = "parameter_value")
    val parameterValue: String
)