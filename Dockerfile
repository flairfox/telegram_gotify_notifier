FROM eclipse-temurin:17.0.7_7-jre-jammy
ENV ARTIFACT_NAME=telegram-gotify-notifier-*.jar
COPY ./build/libs/$ARTIFACT_NAME /tmp/tgn.jar
WORKDIR /tmp
ENTRYPOINT ["java","-jar","tgn.jar"]